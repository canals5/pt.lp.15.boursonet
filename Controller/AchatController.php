<?php
namespace Controller;
class AchatController {
    public function listAchats(){
        $app = \Slim\Slim::getInstance();
        $aff = array();
        $res =array();

        $ac_vide = true;

        $achat = new \Model\Achat();
        $ach=$achat->all();
        if(!$ach->isEmpty()){
            $aff['status']=200;
            $ac_vide=false;
            foreach($ach as $ac){
                $famille = new \Model\Famille();
                $fam = $famille->find($ac->id_famille);
                $res[] = array( 'id' => $ac->id,'famille' => array('id' => $fam->id, 'nom' => $fam->nom, 'prenom' => $fam->prenom_resp , 'link' => 'familles/'.$fam->id), 
                    'date_creation' => $ac->date_creation , 'date_modif' => $ac->date_modif , 'link' => "achat/".$ac->id);
            }
            $aff['achat'] =$res;
        }

        if($ac_vide){
            $aff['status']=500;
            $aff['error']='Aucun achat dans la base.';
        }

        echo json_encode($aff);
    }

    public function detailAchat($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $ac_vide = true;

        $achat = new \Model\Achat();
        $ach=$achat->where('id_famille','=',$id)->get();
        if(!$ach->isEmpty()){
            $aff['status']=200;
            $ac_vide=false;
            $famille = new \Model\Famille();
            $fam = $famille->find($ach[0]->id_famille)->get();
            $paiement = new \Model\Paiement();
            $paie = $paiement->find($ach[0]->id_paiement);
                $aff['achat'] = array( 'id' => $ach[0]->id, 'famille' => array('nom' => $fam[0]->nom, 'prenom' => $fam[0]->prenom_resp , 'link' => 'familles/'.$fam[0]->id), 
                    'date_creation' => $ach[0]->date_creation, 'date_modif' => $ach[0]->date_modif, 
                    'montant' => $ach[0]->montant ,'frais' => $ach[0]->frais, 'solde' => $ach[0]->solde);
        }

        if($ac_vide){
            $aff['status']=500;
            $aff['error']='Id incorrect.';
        }

        echo json_encode($aff);
    }

    public function listAchatFamille($id_fam){
        $app = \Slim\Slim::getInstance();
        $aff = array();
        $res =array();

        $ex_vide = true;

        $exemplaires = new \Model\Exemplaire();
        $exemplaire=$exemplaires->where('id_famille_achat','=',$id_fam)->get();
        if(!$exemplaire->isEmpty()){
            $aff['status']=200;
            $ex_vide=false;
            
            foreach($exemplaire as $ex){
                if ($ex->id_famille_achat != $ex->id_famille_depot) {
                    $manuel = new \Model\Manuel();
                    $man = $manuel->where('isbn','=', $ex->id_manuel)->get();

                    $etats = new \Model\Etat();
                    $etat = $etats->find($ex->id_etat);

                    $prix = new \Model\Prix();
                    $pr = $prix->where('id_manuel','=',$man[0]->id)->orWhere('id_etat','=',$ex->id_etat)->get();

                    if($pr->isEmpty()){
                        $prix->id_manuel = $man[0]->id;
                        $prix->id_etat = $ex->id_etat;
                        $prix->montant = $man[0]->tarif * ($etat->pourcentage/100);
                        $prix->save();
                        $res[] = array( 'id' => $ex->id,'manuel' => array('id' => $man[0]->id, 'titre' => $man[0]->titre, 'tarif' => $man[0]->tarif , 'link' => 'manuel/'.$man[0]->id), 'etat' => array('id' => $etat->id, 'libelle' => $etat->libelle ), 'id_etat' => $ex->id_etat , 'id_famille_depot' => $ex->id_famille_depot,'id_famille_achat' => $ex->id_famille_achat,'id_enfant_depot' => $ex->id_enfant_depot,'id_enfant_achat' => $ex->id_enfant_achat,'prix' => $prix->montant, 'link' => "exemplaire/".$ex->id);
                    } else {
                        $res[] = array( 'id' => $ex->id,'manuel' => array('id' => $man[0]->id, 'titre' => $man[0]->titre, 'tarif' => $man[0]->tarif , 'link' => 'manuel/'.$man[0]->id), 'etat' => array('id' => $etat->id, 'libelle' => $etat->libelle ), 'id_etat' => $ex->id_etat , 'id_famille_depot' => $ex->id_famille_depot,'id_famille_achat' => $ex->id_famille_achat,'id_enfant_depot' => $ex->id_enfant_depot,'id_enfant_achat' => $ex->id_enfant_achat,'prix' => $pr[0]->montant, 'link' => "exemplaire/".$ex->id);
                    }  
                }
            }
            $aff['achat'] = $res;
        }

        if($ex_vide){
            $aff['status']=500;
            $aff['error']='Aucun achat pour cette famille.';
        }

        echo json_encode($aff);
    }

    public function ajoutAchat(){
        $app = \Slim\Slim::getInstance();
        $aff = array();
        $ex_vide = true;

        if(isset($_POST['id_famille']) && isset($_POST['id_exemp'])){
            $exemplaires = new \Model\Exemplaire();
            $exemplaire=$exemplaires->where('id','=',$_POST['id_exemp'])->get();
            if($exemplaire->isEmpty()){
                $aff['status'] = 500;
                $aff['error'] = 'Ce code d\'exemplaire n\'existe pas.';
            }else{
                if(!is_null($exemplaire[0]->id_famille_achat)){
                    $aff['status'] = 500;
                    $aff['error'] = 'L\'exemplaire a déjà été acheté.';
                }else{
                    if ($exemplaire[0]->id_famille_depot == $_POST['id_famille']) {
                        $aff['status'] = 500;
                        $aff['error'] = 'L\'exemplaire appartient à cette famille.'; 
                    } else {
                        $exemplaire[0]->id_famille_achat = $_POST['id_famille'];
                        $exemplaire[0]->id_enfant_achat = ($_POST['id_enfant'] != "") ? $_POST['id_enfant'] : null;
                        $exemplaire[0]->save();
                        $id_famille_depot = $exemplaire[0]->id_famille_depot;
                        

                        $manuel = new \Model\Manuel();
                        $man = $manuel->where('isbn', '=', $exemplaire[0]->id_manuel)->get();

                        $prix = new \Model\Prix();
                        $pr = $prix->where('id_manuel','=',$man[0]->id)->orWhere('id_etat','=',$exemplaire[0]->id_etat)->get();
                        $famille = new \Model\Famille();
                        $famille_depot = $famille->where('id', '=', $id_famille_depot)->get();
                        $famille_achat = $famille->where('id', '=', $_POST['id_famille'])->get();
                        $taux_dossier = 0;
                        $taux_dossier_achat = 0;
                        $taux = new \Model\Taux();
                        $tx = $taux->where('id', '=', 1)->get();
                        if (!$tx->isEmpty() && $famille_depot[0]->frais == 0) {
                            $taux_dossier = $tx[0]->frais_dossier;
                        }
                        if (!$tx->isEmpty() && $famille_achat[0]->frais == 0) {
                            $taux_dossier_achat = $tx[0]->frais_dossier;
                        }
                        //dossier depot
                        $dossier_depot = new \Model\Depot();
                        $dossier = $dossier_depot->where('id_famille', '=', $id_famille_depot)->get();
                        $dossier[0]->montant_vendu = $dossier[0]->montant_vendu + $pr[0]->montant;
                        $dossier[0]->frais = $dossier[0]->montant_vendu * ($taux_dossier/100);
                        $dossier[0]->save();

                        //dossier achat
                        $dossier_achat = new \Model\Achat();
                        $doss_achat = $dossier_achat->where('id_famille', '=', $_POST['id_famille'])->get();
                        if ($doss_achat->isEmpty()) {
                            $dossier_achat->id_famille = $_POST['id_famille'];
                            $dossier_achat->date_creation = date('Y-m-d');
                            $dossier_achat->date_modif = date('Y-m-d');
                            $dossier_achat->montant = $pr[0]->montant;
                            $dossier_achat->solde = $pr[0]->montant;
                            $dossier_achat->frais = $dossier_achat->montant * ($taux_dossier_achat/100);
                            $dossier_achat->save();
                        } else {
                            $doss_achat[0]->date_modif = date('Y-m-d');
                            $doss_achat[0]->montant = $doss_achat[0]->montant + $pr[0]->montant;
                            $doss_achat[0]->solde = $doss_achat[0]->solde + $pr[0]->montant;
                            $doss_achat[0]->frais = $doss_achat[0]->montant * ($taux_dossier_achat/100);
                            $doss_achat[0]->save();
                        }

                        $aff['status'] = 200;
                        $aff['message'] = 'Exemplaire ajouté dans la liste d\'achats avec succès.';
                    }
                }
            }
        } else {
            $aff['status'] = 500;
            $aff['error'] = 'Paramètres manquants.';
        }
        echo json_encode($aff);
    }

    public function paiementAchat()
    {
        $aff = array();
        if (isset($_POST['id_famille']) && isset($_POST['montant']) && isset($_POST['mode'])) {
            $achat = new \Model\Achat();
            $ach=$achat->where('id_famille','=',$_POST['id_famille'])->get();
            if (!$ach->isEmpty()) {
                if ($_POST['montant'] > ($ach[0]->solde + $ach[0]->frais)) {
                    $aff['status'] = 500;
                    $aff['error'] = "Le paiement ne peut pas dépasser le montant à payer.";
                } else {
                    $ach[0]->solde = $ach[0]->solde - $_POST['montant'];
                    $ach[0]->save();
                    $paiement = new \Model\Paiement();
                    $paiement->id_achat = $_POST['id_famille'];
                    $paiement->montant = $_POST['montant'];
                    $paiement->date = date('Y-m-d');
                    $paiement->id_mode_paiement = $_POST['mode'];
                    $paiement->save();
                    $aff['status'] = 200;
                    $aff['message'] = "Paiement enregistré avec succès !";
                }
            }
        } else {
            $aff['status'] = 500;
            $aff['error'] = "Paramètres manquants.";
        }

        echo json_encode($aff);
    }


}