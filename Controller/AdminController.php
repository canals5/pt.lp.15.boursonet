<?php
namespace Controller;
class AdminController {
    public function index()
    {
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('admin.html.twig');
        $tmpl->display(array());
    }

    public function tableauDeBord() {
        $aff = array();
        $exemplaires = new \Model\Exemplaire();
        $familles = new \Model\Famille();

        $allexemplaires = $exemplaires -> all();
        $nb_exemplaires = $allexemplaires -> count();        
        $nb_exemplaires_vendus = 0 ;
        $nb_exemplaires_rendus = 0 ;
        foreach ($allexemplaires as $exemplaire) {
            if ($exemplaire -> id_famille_achat == $exemplaire -> id_famille_depot) {
                $nb_exemplaires_rendus++;
            } else {
                if ($exemplaire -> id_famille_achat != null) {
                    $nb_exemplaires_vendus++;
                }
            }
        }

        $allfamilles = $familles -> all();
        $nb_familles = $allfamilles -> count() ;

        $depots = new \Model\Depot();
        $alldepots = $depots -> all();
        $nb_familles_depots = $alldepots -> count();
        $total_depots = 0;
        $total_vendus = 0;
        foreach($alldepots as $depot) {
            $total_depots += $depot -> montant;
            $total_vendus += $depot ->montant_vendu;
        }

        $achats = new \Model\Achat();
        $nb_familles_achats = $achats -> all() -> count();

        $paiements = new \Model\Paiement();
        $allpaiements = $paiements -> all();
        $total_paiements = 0;
        foreach($allpaiements as $paiement) {
            $total_paiements += $paiement -> montant;
        }

        $aff['exemplaires'] = $nb_exemplaires;
        $aff['exemplaires_vendus'] = $nb_exemplaires_vendus;
        $aff['exemplaires_rendus'] = $nb_exemplaires_rendus;

        $aff['familles'] = $nb_familles;
        $aff['familles_depots'] = $nb_familles_depots;
        $aff['familles_achats'] = $nb_familles_achats;

        $aff['total_depots'] = $total_depots;
        $aff['total_vendus'] = $total_vendus;

        $aff['total_paiement'] = $total_paiements;

        echo json_encode($aff);
    }
}