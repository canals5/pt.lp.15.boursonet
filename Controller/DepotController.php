<?php
namespace Controller;

class DepotController{
    public function listDepots(){
        $app = \Slim\Slim::getInstance();
        $aff = array();
        $res =array();
        $dep_vide = true;

        $depot = new \Model\Depot();
        $dep=$depot->all();
        if(!$dep->isEmpty()){
            $aff['status']=200;
            $dep_vide=false;
            
            foreach($dep as $de){
                $famille = new \Model\Famille();
                $fam = $famille->find($de->id_famille);
                $res[] = array( 'id' => $de->id,'famille' => array('id' => $fam->id, 'nom' => $fam->nom, 'prenom' => $fam->prenom_resp , 'link' => 'familles/'.$fam->id), 
                    'date_cree' => $de->date_cree , 'date_dernier_ajout' => $de->date_dernier_ajout, 'link' => "depots/".$de->id);
                
            }
            $aff['depot'] = $res;
        }

        if($dep_vide){
            $aff['status']=500;
            $aff['error']='Aucun depot dans la base.';
        }

        echo json_encode($aff);
    }

    public function detaildepot($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $dep_vide = true;

        $depot = new \Model\Depot();
        $dep=$depot->where('id_famille','=',$id)->get();
        if(!$dep->isEmpty()){
            $aff['status']=200;
            $dep_vide=false;
            $famille = new \Model\Famille();
            $fam = $famille->find($dep[0]->id_famille)->get();
            $frais_envoi = 0;

            $taux = new \Model\Taux();
            $tx = $taux->where('id', '=', 1)->get();
            if (!$tx->isEmpty() && $dep[0]->frais_envoi == 1) {
                $frais_envoi = $tx[0]->frais_envoi;
            }

            $aff['depot'] = array( 'id' => $dep[0]->id,'famille' => array('id' =>$fam[0]->id, 'nom' => $fam[0]->nom, 'prenom' => $fam[0]->prenom_resp , 'link' => 'familles/'.$fam[0]->id), 
                    'date_cree' => $dep[0]->date_cree , 'date_dernier_ajout' => $dep[0]->date_dernier_ajout , 'frais' =>$dep[0]->frais , 'montant' => $dep[0]->montant,'montant_vendu' => $dep[0]->montant_vendu,'montant_paye' => $dep[0]->montant_paye,'frais_envoi' => $frais_envoi, 'appliquer_frais' => $dep[0]->frais_envoi);
        }

        if($dep_vide){
            $aff['status']=500;
            $aff['error']='Id incorrect.';
        }

        echo json_encode($aff);
    }

    public function listDepotsFamille($id_fam){
        $app = \Slim\Slim::getInstance();
        $aff = array();
        $res =array();

        $ex_vide = true;

        $exemplaires = new \Model\Exemplaire();
        $exemplaire=$exemplaires->where('id_famille_depot','=',$id_fam)->get();
        if(!$exemplaire->isEmpty()){
            $aff['status']=200;
            $ex_vide=false;
            
            foreach($exemplaire as $ex){
                $manuel = new \Model\Manuel();
                $man = $manuel->where('isbn','=', $ex->id_manuel)->get();

                $etats = new \Model\Etat();
                $etat = $etats->where('id', '=', $ex->id_etat)->get();

                $prix = new \Model\Prix();
                $pr = $prix->where('id_manuel','=',$man[0]->id)->where('id_etat','=',$ex->id_etat)->get();

                if($pr->isEmpty()){
                    $prix->id_manuel = $man[0]->id;
                    $prix->id_etat = $ex->id_etat;
                    $prix->montant = $man[0]->tarif * ($etat[0]->pourcentage/100);
                    $prix->save();

                    $res[] = array( 'id' => $ex->id,'manuel' => array('isbn' => $man[0]->isbn, 'titre' => $man[0]->titre, 'tarif' => $man[0]->tarif , 'link' => 'manuel/'.$man[0]->id), 
                    'etat' => array('id' => $etat[0]->id, 'libelle' => $etat[0]->libelle ), 'id_famille_depot' => $ex->id_famille_depot,'id_famille_achat' => $ex->id_famille_achat,'id_enfant_depot' => $ex->id_enfant_depot,
                    'id_enfant_achat' => $ex->id_enfant_achat,'prix' => $prix->montant, 'link' => "exemplaire/".$ex->id);
                }else{
                    $res[] = array( 'id' => $ex->id,'manuel' => array('isbn' => $man[0]->isbn, 'titre' => $man[0]->titre, 'tarif' => $man[0]->tarif , 'link' => 'manuel/'.$man[0]->id), 
                        'etat' => array('id' => $etat[0]->id, 'libelle' => $etat[0]->libelle ), 'id_famille_depot' => $ex->id_famille_depot,'id_famille_achat' => $ex->id_famille_achat,'id_enfant_depot' => $ex->id_enfant_depot,
                        'id_enfant_achat' => $ex->id_enfant_achat,'prix' => $pr[0]->montant, 'link' => "exemplaire/".$ex->id);
                }
            }
            $aff['depot'] = $res;
        }

        if($ex_vide){
            $aff['status']=500;
            $aff['error']='Aucun dépôt pour cette famille.';
        }

        echo json_encode($aff);
    }

    public function appliquerFraisEnvoi()
    {
        $aff = array();
        if (isset($_POST['id_famille']) && isset($_POST['frais'])) {
            $depot = new \Model\Depot();
            $dep=$depot->where('id_famille','=',$_POST['id_famille'])->get();
            if (!$dep->isEmpty()) {
                $dep[0]->frais_envoi = $_POST['frais'];
                $dep[0]->save();
                $aff['status'] = 200;
            }
        } else {
            $aff['status'] = 500;
            $aff['error'] = "Paramètres manquants.";
        }
        echo json_encode($aff);
    }

    public function paiementDepot()
    {
        $aff = array();
        if (isset($_POST['id_famille']) && isset($_POST['montant'])) {
            $depot = new \Model\Depot();
            $dep=$depot->where('id_famille','=',$_POST['id_famille'])->get();
            if (!$dep->isEmpty()) {
                $frais_envoi = 0;
                $taux = new \Model\Taux();
                $tx = $taux->where('id', '=', 1)->get();
                if (!$tx->isEmpty() && $dep[0]->frais_envoi == 1) {
                    $frais_envoi = $tx[0]->frais_envoi;
                }

                $total = $dep[0]->montant_vendu - $dep[0]->frais - $frais_envoi;
                if ($_POST['montant'] > $total) {
                    $aff['status'] = 500;
                    $aff['error'] = "Le paiement ne peut pas dépasser le montant à payer (" . $total . " €)";
                } else {
                    $dep[0]->montant_paye = $dep[0]->montant_paye + $_POST['montant'];
                    $dep[0]->save();
                    $aff['status'] = 200;
                    $aff['message'] = "Paiement enregistré avec succès !";
                }
            }
        } else {
            $aff['status'] = 500;
            $aff['error'] = "Paramètres manquants.";
        }

        echo json_encode($aff);
    }
}