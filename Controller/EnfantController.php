<?php
namespace Controller;
class EnfantController {
	public function listEnfants(){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$enf_vide = true;

		$enfant = new \Model\Enfant();
		$enf=$enfant->all();
		if(!$enf->isEmpty()){
			$aff['status']=200;
			$enf_vide=false;
			$i =0;
			foreach($enf as $en){
				$famille = new \Model\Famille();
				$fam = $famille->find($en->id_famille);
				$aff['enfant '.$i] = array( 'id' => $en->id,'nom' => $en->nom ,'prenom' => $en->prenom ,'famille' => array('nom' => $fam->nom, 'prenom_resp' => $fam->prenom_resp , 'link' => 'familles/'.$fam->id), 
				 'classe' => $en->classe, 'link' => "enfants/".$en->id);
				$i++;
			}
		}

		if($enf_vide){
			$aff['status']=500;
			$aff['error']='Aucun enfant dans la base.';
		}

		echo json_encode($aff);
	}

	public function detailEnfant($id){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$enf_vide = true;

		$enfant = new \Model\Enfant();
		$enf=$enfant->where('id','=',$id)->get();
		if(!$enf->isEmpty()){
			$aff['status']=200;
			$enf_vide=false;
			$famille = new \Model\Famille();
			$fam = $famille->find($enf[0]->id_famille)->get();
			$aff['enfant'] = array( 'id' => $enf[0]->id,'nom' => $enf[0]->nom ,'prenom' => $enf[0]->prenom ,'famille' => array('nom' => $fam[0]->nom, 'prenom_resp' => $fam[0]->prenom_resp , 'link' => 'familles/'.$fam[0]->id), 
				 'classe' => $enf[0]->classe);		
		}

		if($enf_vide){
			$aff['status']=500;
			$aff['error']='Id incorrect.';
		}

		echo json_encode($aff);
	}

	public function listEnfantsFamille($id) {
		$aff = array() ;
		$en = array();
		$contenu = true ;
		$enfants = new \Model\Enfant() ;
		$familles = new \Model\Famille() ;		

		$enfant = $enfants -> where('id_famille', '=', $id) -> get() ;
		$famille = $familles -> where('id', '=', $id) -> get() ;

		if ( (! $enfant -> isEmpty()) && (! $famille -> isEmpty()) ) {
			$aff['status'] = 200 ;	

			$infosFamille = array('nom' => $famille[0]->nom, 
							   	  'prenom_resp' => $famille[0]->prenom_resp, 
							   	  'link' => 'familles/'.$famille[0]->id) ;		

			foreach($enfant as $enf) {
				$en[] = array('id' => $enf->id,
							   'nom' => $enf->nom,
							   'prenom' => $enf->prenom,
							   'classe' => $enf->classe,
							   'famille' => $infosFamille );
			}
			$aff['enfants'] = $en;

		} else {

			$contenu = false ; 
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['error'] = 'Aucun enfant ou aucune famille' ;
		}

		echo json_encode($aff) ;
	}

	public function verifAjoutEnfant() {
		$aff = array() ;
		$contenu = true ;
		$enfant = new \Model\Enfant() ;
		$familles = new \Model\Famille() ;

		if (isset ($_POST['nom']) && isset ($_POST['prenom']) && 
			isset ($_POST['classe']) && isset ($_POST['famille'])) {

			$famille = $familles -> where('id', '=', $_POST['famille']) -> get() ;
			if (! $famille -> isEmpty()) {
				$enfant->nom = htmlspecialchars($_POST['nom']);
	            $enfant->prenom = htmlspecialchars($_POST['prenom']);
	            $enfant->classe = htmlspecialchars($_POST['classe']);
	            $enfant->id_famille = htmlspecialchars($_POST['famille']);
	            $enfant->save();

	            $aff['status'] = 200 ;
				$aff['message'] = 'Enfant ajouté avec succès.' ;
	        } else {
	        	$contenu = false ;
	        }

		} else {
			$contenu = false ;
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['error'] = 'Enfant non inséré.' ;
		} 

		echo json_encode($aff) ;
	}

	public function nombreEnfants($id) {
		$aff = array() ;
		$contenu = true ;
		$enfants = new \Model\Enfant() ;
		$familles = new \Model\Famille() ;

		$famille = $familles -> where('id', '=', $id) -> get() ;
		if (! $famille -> isEmpty()) {
			$aff['status'] = 200 ;
			$nombre = $enfants -> where('id_famille', '=', $id) -> count();
			$aff['nombre'] = $nombre ;

		} else {
			$contenu = false ;
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['error'] = 'Pas de famille.';
		}

		echo json_encode($aff);
	}

	public function supprimerEnfants($id_fam,$id_enf){
		$aff = array();
		$enfants = new \Model\Enfant() ;
		$enfant = $enfants->where('id','=',$id_enf)->get();

		$exemplaires = new \Model\Exemplaire();
		$exemplaire_depot = $exemplaires->where('id_enfant_depot','=',$id_enf)->get();
		$exemplaire_achat = $exemplaires->where('id_enfant_achat','=',$id_enf)->get();

		if(!$enfant -> isEmpty()){
			if($enfant[0]->id_famille == $id_fam){
				$enfant[0]->delete();
				if(!$exemplaire_depot->isEmpty()){
					foreach ($exemplaire_depot as $ex) {
						$ex->id_enfant_depot = null;
						$ex->save();
					}
				}
				if(!$exemplaire_achat->isEmpty()){
					foreach ($exemplaire_achat as $exe) {
						$exe->id_enfant_achat = null;
						$exe->save();
					}
				}
				$aff['status'] = 200 ;
				$aff['message'] = 'Enfant supprimé avec succès.' ;
			}else{
				$aff['status'] = 500 ; 
				$aff['message'] = 'erreur dans la suppression.';
			}
		}else{
			$aff['status'] = 500 ; 
			$aff['message'] = 'erreur dans la suppression.';
		}
		echo json_encode($aff);
	}

	public function modifierEnfant() {
		$aff = array() ;
		$contenu = true ;
		$enfants = new \Model\Enfant() ;
		
		$app = \Slim\Slim::getInstance(); 
		$request = $app -> request() ;
		$var = $request -> put() ;

		if (isset ($var['nom']) && isset ($var['prenom']) && isset ($var['classe'])) {

			$enfant = $enfants -> where('id', '=', $var['id']) -> get() ;

			if (! $enfant -> isEmpty()) {

				if ($enfant[0] -> id_famille == $var["id_famille"]) {

					$enfant[0] -> nom = $var['nom'];
					$enfant[0] -> prenom = $var['prenom'];
					$enfant[0] -> classe = $var['classe'];
					$enfant[0] -> save() ;

				} else {

					$contenu = false; 
				}

			} else {

				$contenu = false ;
			}

		} else {
			$contenu = false;
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['erreur'] = 'Erreur de modification.';
		} else {
			$aff['status'] = 200 ;
			$aff['message'] = "Modification effectuée avec succès.";
		}

		echo json_encode($aff);
	}
}
