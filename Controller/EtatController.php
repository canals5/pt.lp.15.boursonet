<?php
namespace Controller;
class EtatController {
	public function listEtat(){
		$app = \Slim\Slim::getInstance();
		$aff = array();
		$eta = array();
		$etat_vide = true;

		$etat = new \Model\Etat();
		$et=$etat->all();
		if(!$et->isEmpty()){
			$aff['status'] = 200;
			$etat_vide=false;
			$i =0;
			foreach($et as $e){
				$eta[] = array( 'id' => $e->id,'libelle' => $e->libelle ,'pourcentage' => $e->pourcentage);
				$i++;
			}
			$aff['etats'] = $eta;
		}

		if($etat_vide){
			$aff['status'] = 500;
			$aff['error'] = 'Aucun état dans la base.';
		}

		echo json_encode($aff);
	}

	public function detailEtat($id){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$etat_vide = true;

		$etat = new \Model\etat();
		$et=$etat->where('id','=',$id)->get();
		if(!$et->isEmpty()){
			$aff['status']=200;
			$etat_vide=false;
			$aff['etat'] = array( 'id' => $et[0]->id,'libelle' => $et[0]->libelle ,'pourcentage' => $et[0]->pourcentage );		
		}

		if($etat_vide){
			$aff['status']=500;
			$aff['error']='Id incorrect.';
		}

		echo json_encode($aff);
	}

	public function ajouterEtat() {
		$aff = array() ;
		$newetat = new \Model\Etat() ;
		$etats = new \Model\Etat() ;
		$manuels = new \Model\Manuel();

		if (isset($_POST['libelle']) && isset($_POST['pourcentage']) && isset($_POST['id'])) {
			
			$etat = $etats -> where('id', '=', $_POST['id']) -> get() ;
			if ($etat -> isEmpty()) {
				$newetat->id = $_POST['id'];
				$newetat->libelle = $_POST['libelle'];
				$newetat->pourcentage = $_POST['pourcentage'];
				$newetat->save() ;

				$allmanuel = $manuels -> all();
				foreach ($allmanuel as $manuel) {
					$prix = new \Model\Prix();
					$prix -> id_manuel = $manuel->id;
					$prix -> id_etat = $newetat->id;
					$prix -> montant = $manuel->tarif * ($newetat->pourcentage/100);
					$prix->save();
				}

				$aff['status'] = 200;
				$aff['message'] = "Etat ajouté avec succès.";
			} else {
				$aff['status'] = 500 ;
				$aff['error'] = "Id état déjà existant.";
			}
		} else {
			$aff['status'] = 500 ;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}

	public function modifierEtat() {
		$aff = array() ;
		$etats = new \Model\Etat() ;
		$prix = new \Model\Prix();
		$manuels = new \Model\Manuel();

		$app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

		if (isset($var['id']) && isset($var['libelle']) && isset($var['pourcentage'])) {

			$etat = $etats -> where('id', '=', $var['id']) -> get() ;
			if (! $etat -> isEmpty()) {
				$etat[0]->id = $var['id'];
				$etat[0]->libelle = $var['libelle'];
				$etat[0]->pourcentage = $var['pourcentage'];
				$etat[0]->save() ;

				$allprix = $prix -> where('id_etat', '=', $etat[0]->id) -> get();
				if (! $allprix -> isEmpty()) {
					foreach($allprix as $eachprix) {
						$manuel = $manuels -> where('id', '=', $eachprix->id_manuel) -> get();
						if (! $manuel -> isEmpty()) {
							$eachprix->montant = $manuel[0]->tarif * ($etat[0]->pourcentage/100);
							$eachprix->save();
						}
					}
				}

				$aff['status'] = 200;
				$aff['message'] = "Etat modifié avec succès.";

			} else {
				$aff['status'] = 500 ;
				$aff['error'] = "Etat introuvable.";
			}
		} else {
			$aff['status'] = 500 ;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}
}
