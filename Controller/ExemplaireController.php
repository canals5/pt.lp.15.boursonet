<?php
namespace Controller;
class ExemplaireController {
    public function listExemplaire(){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $exemp_vide = true;

        $exemplaire = new \Model\Exemplaire();
        $exemp=$exemplaire->all();
        if(!$exemp->isEmpty()){
            $aff['status']=200;
            $exemp_vide=false;
            $i =0;
            foreach($exemp as $ex){
                $manuel = new \Model\Manuel();
                $man = $manuel->find($ex->id_manuel);
                $aff['exemplaire '.$i] = array( 'id' => $ex->id,'id_manuel' => $ex->id_manuel,'id_etat' => $ex->id_etat, 'id_famille_depot' => $ex->id_famille_depot,'id_famille_achat' => $ex->id_famille_achat,'id_enfant_depot' => $ex->id_enfant_depot,'id_enfant_achat' => $ex->id_enfant_achat
                    ,'date_depot' => $ex->date_depot,'date_achat' => $ex->date_achat);
                $i++;
            }
        }

        if($exemp_vide){
            $aff['status']=500;
            $aff['error']='Aucun exemplaire dans la base.';
        }

        echo json_encode($aff);
    }

    public function detailExemplaire($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $exemp_vide = true;

        $exemplaire = new \Model\Exemplaire();
        $exemp=$exemplaire->where('id','=',$id)->get();
        if(!$exemp->isEmpty()){
            $aff['status']=200;
            $exemp_vide=false;
            $manuel = new \Model\Manuel();
            $man = $manuel->find($exemp[0]->id_manuel)->get();
            $aff['exemplaire'] = array( 'id' => $exemp[0]->id,'id_manuel' => $exemp[0]->id_manuel,'id_etat' => $exemp[0]->id_etat, 'id_famille_depot' => $exemp[0]->id_famille_depot,'id_famille_achat' => $exemp[0]->id_famille_achat,'id_enfant_depot' => $exemp[0]->id_enfant_depot,'id_enfant_achat' => $exemp[0]->id_enfant_achat
                    ,'date_depot' => $exemp[0]->date_depot,'date_achat' => $exemp[0]->date_achat);
        }

        if($exemp_vide){
            $aff['status']=500;
            $aff['error']='Id incorrect.';
        }

        echo json_encode($aff);
    }

    public function ajoutExemplaire() {
        $aff = array() ;
        $manuels = new \Model\Manuel() ;
        $etats = new \Model\Etat() ;
        $exemplaire = new \Model\Exemplaire() ;
        $familles = new \Model\Famille() ;

        if (isset($_POST['isbn_manuel']) && isset($_POST['id_etat']) && isset($_POST['id_famille'])) {
            $manuel = $manuels -> where('isbn', '=', $_POST['isbn_manuel']) -> get() ;
            $famille = $familles -> where('id', '=', $_POST['id_famille']) -> get() ;
            $etat = $etats -> where('id', '=', $_POST['id_etat']) -> get() ;
            $exem = $exemplaire->where('id', '=', $_POST['id_exemplaire'])->get();
            if (! $manuel -> isEmpty()) {
                if ($exem->isEmpty()) {
                    if (! $etat -> isEmpty()) {

                        if (! $famille -> isEmpty()) {
                            $exemplaire->id = $_POST['id_exemplaire'];
                            $exemplaire->id_manuel = $_POST['isbn_manuel'];
                            $exemplaire->id_etat = $_POST['id_etat'];
                            $exemplaire->id_famille_depot = $_POST['id_famille'];
                            $exemplaire->id_enfant_depot = ($_POST['id_enfant'] != "") ? $_POST['id_enfant'] : null;
                            $exemplaire->date_depot = date('Y-m-d');
                            $exemplaire->save() ;

                            //dossier de depot
                            $prix = new \Model\Prix();
                            $pr = $prix->where('id_manuel','=',$manuel[0]->id)->where('id_etat','=',$exemplaire->id_etat)->get();

                            if($pr->isEmpty()){
                                $prix->id_manuel = $manuel[0]->id;
                                $prix->id_etat = $exemplaire->id_etat;
                                $prix->montant = $manuel[0]->tarif * ($etat[0]->pourcentage/100);
                                $prix->save();
                                $prix_exemplaire = $prix->montant;
                            } else {
                                $prix_exemplaire = $pr[0]->montant;
                            }

                            $taux_dossier = 0;
                            $taux = new \Model\Taux();
                            $tx = $taux->where('id', '=', 1)->get();
                            if (!$tx->isEmpty() && $famille[0]->frais == 0) {
                                $taux_dossier = $tx[0]->frais_dossier;
                            }

                            $dossier_depot = new \Model\Depot();
                            $dossier = $dossier_depot->where('id_famille', '=', $_POST['id_famille'])->get();
                            if ($dossier->isEmpty()) {
                                $dossier_depot->id_famille = $_POST['id_famille'];
                                $dossier_depot->date_cree = date('Y-m-d');
                                $dossier_depot->date_dernier_ajout = date('Y-m-d');
                                $dossier_depot->montant = $prix_exemplaire;
                                $dossier_depot->frais = $dossier_depot->montant_vendu * ($taux_dossier/100);
                                $dossier_depot->save();
                            } else {
                                $dossier[0]->date_dernier_ajout = date('Y-m-d');
                                $dossier[0]->montant = $dossier[0]->montant + $prix_exemplaire;
                                $dossier[0]->frais = $dossier[0]->montant_vendu * ($taux_dossier/100);
                                $dossier[0]->save();
                            }

                            $aff['status'] = 200;
                            $aff['message'] = 'Exemplaire ajouté avec succès.';

                        } else {
                            $aff['status'] = 500;
                            $aff['error'] = "Cette famille n'existe pas.";
                        }

                    } else {
                        $aff['status'] = 500;
                        $aff['error'] = "Ce code d'état n'existe pas.";
                    }
                } else {
                    $aff['status'] = 500;
                    $aff['error'] = "Ce code d'exemplaire est déjà utilisé.";
                }
            } else {
                $aff['status'] = 500;
                $aff['error'] = "Ce code de manuel n'existe pas.";
            }
        }

        echo json_encode($aff);
    }

    public function supprimerExemplaire() {
        $aff = array();
        $exemplaires = new \Model\Exemplaire() ;

        $app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> delete() ;

        if (isset($var['id'])) {

            $exemplaire = $exemplaires -> where('id', '=', $var['id']) -> get();
            if (! $exemplaire -> isEmpty()) {

                if ($exemplaire[0]->id_famille_depot == $exemplaire[0]->id_famille_achat || 
                    $exemplaire[0]->id_famille_achat == null) {
                    
                    $exemplaire[0]->delete();
                    $aff['status'] = 200;
                    $aff['message'] = "Exemplaire supprimé avec succès.";

                } else {
                    $aff['status'] = 500;
                    $aff['error'] = "Impossible de supprimer un exemplaire déjà vendu.";
                }

            } else {
                $aff['status'] = 500;
                $aff['error'] = "Exemplaire inexistant.";
            }
            
        } else {
            $aff['status'] = 500;
            $aff['error'] = "Id manquante.";
        }

        echo json_encode($aff);
    }
}