<?php
namespace Controller;
class ListController {
	public function listListeManuel(){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$liste_vide = true;

		$liste = new \Model\Liste();
		$li=$liste->all();
		if(!$li->isEmpty()){
			$aff['status']=200;
			$liste_vide=false;
			$i =0;
			foreach($li as $l){
				$aff['liste '.$i] = array( 'id' => $l->id,'nom' => $l->nom, 'classe' => $l->classe, 'manuels' => $l->manuel);
				$i++;
			}
		}

		if($liste_vide){
			$aff['status']=500;
			$aff['error']='Aucune liste dans la base.';
		}

		echo json_encode($aff);
	}

	public function detailListeManuel($id){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$liste_vide = true;

		$liste = new \Model\Liste();
		$li=$liste->where('id','=',$id)->get();
		if(!$li->isEmpty()){
			$aff['status']=200;
			$liste_vide=false;
			$aff['liste'] = array( 'id' => $li[0]->id,'nom' => $li[0]->nom, 'classe' => $li[0]->classe, 'manuels' => $li[0]->manuel);
		}

		if($liste_vide){
			$aff['status']=500;
			$aff['error']='Id incorrect ou inexistant.';
		}

		echo json_encode($aff);
	}
}