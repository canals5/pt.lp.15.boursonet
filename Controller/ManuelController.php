<?php
namespace Controller;

class ManuelController {

    public function manuel($id)
    {
        $user = new \Model\User();
        $user_logged = $user->where("login", "=", $_SESSION['user'])->get();

        $man = new \Model\Manuel();
        $manuel = $man->where('id','=',$id)->get();
        if (!$manuel->isEmpty()){

        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirect('/');
        }

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $twig->addGlobal('user', $user_logged[0]);
        $tmpl = $twig->loadTemplate('manuel.html.twig');
        $tmpl->display(array('manuel' => $manuel[0]));
    }

	public function infosManuel($id) {
		$manuels = new \Model\Manuel() ;
		$aff = array() ;
		$contenu = false ;

		$manuel = $manuels -> where('id', '=', $id) -> get() ;
		if (! $manuel -> isEmpty()) {
			$aff['status'] = 200 ;
			$contenu = true ;
			$aff[] = array('id' => $manuel[0] -> id,
						   'isbn' => $manuel[0] -> isbn,
						   'titre' => $manuel[0] -> titre,
						   'matiere' => $manuel[0] -> matiere,
						   'classe' => $manuel[0] -> classe,
						   'tarif' => $manuel[0] -> tarif,
						   'editeur' => $manuel[0] -> editeur,
						   'annee' => $manuel[0] -> annee) ;
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['error'] = "Id inconnu." ;
		}

		echo json_encode($aff) ;
	}

	public function infosListeManuels() {
		$liste_manuels = new \Model\Manuel() ;
		$aff = array() ;
		$contenu = false ;

		$manuels = $liste_manuels -> all() ;
		if (! $manuels -> isEmpty()) {
			$aff['status'] = 200 ;
			$contenu = true ;

			foreach ($manuels as $manuel) {
				$aff[] = array('id' => $manuel -> id,
							   'isbn' => $manuel -> isbn,
							   'titre' => $manuel -> titre,
							   'matiere' => $manuel[0] -> matiere,
						   	   'classe' => $manuel[0] -> classe,
							   'tarif' => $manuel -> tarif,
							   'editeur' => $manuel -> editeur,
							   'annee' => $manuel[0] -> annee) ;
			}
		}

		if (! $contenu) {
			$aff['status'] = 500 ;
			$aff['error'] = "Aucun manuel." ;
		}

		echo json_encode($aff) ;

	}

	public function rechercheManuel(){
		$aff = array();
		$ligne = array();
		$m_vide = true;
		$manuels = new \Model\Manuel();

		if($_POST['isbn']!="" && $_POST['titre']!=""){
			$manuel = $manuels->where('isbn','=',$_POST['isbn'])->where('titre','like','%'.$_POST['titre'].'%')->get();
		}
		if($_POST['isbn'] =='' && $_POST['titre'] != ''){
			$manuel = $manuels->where('titre','LIKE','%'.$_POST['titre'].'%')->get();
			}
		if($_POST['titre'] == '' && $_POST['isbn'] != ''){
			$manuel = $manuels->where('isbn','=',$_POST['isbn'])->get();
		}
		if(!$manuel->isEmpty()){
			$aff['status']=200;
			$m_vide=false;
			foreach($manuel as $m){
				$ligne[] = array('id' => $m->id,
							   'isbn' => $m->isbn,
							   'titre' => $m->titre,
							   'matiere' => $m->matiere,
							   'classe' => $m->classe,
							   'tarif' => $m->tarif,
							   'editeur' => $m->editeur,
							   'annee' => $m->annee) ;
			}
			$aff['manuel'] = $ligne;
		}

		if($m_vide){
			$aff['status']=500;
			$aff['error']='Aucun manuel ne correspond à cette recherche...';
		}

		echo json_encode($aff);
	}

	public function ajoutManuel() {
		$aff = array();
		$etats = new \Model\Etat() ;
		$newmanuel = new \Model\Manuel() ;
		$manuels = new \Model\Manuel() ;

		if (isset($_POST['isbn']) && isset($_POST['titre']) && 
			isset($_POST['editeur']) && isset($_POST['tarif']) &&
			isset($_POST['classe']) && isset($_POST['matiere']) && isset($_POST['annee'])) {

			$manuel = $manuels -> where('isbn', '=', $_POST['isbn']) -> get() ;
			if ($manuel -> isEmpty()) {

				$newmanuel->isbn = $_POST['isbn'];
				$newmanuel->titre = htmlspecialchars($_POST['titre']);				
				$newmanuel->matiere = $_POST['matiere'];
				$newmanuel->classe = $_POST['classe'];
				$newmanuel->tarif = $_POST['tarif'];
				$newmanuel->editeur = htmlspecialchars($_POST['editeur']);
				$newmanuel->annee = $_POST['annee'];
				$newmanuel->save();

				$all_etat = $etats->all();
				foreach ($all_etat as $etat) {
					$prix = new \Model\Prix() ;
					$prix->id_manuel = $newmanuel->id;
					$prix->id_etat = $etat->id;
					$prix->montant = $newmanuel->tarif * ($etat->pourcentage/100);
					$prix->save();
				};

				$aff['status'] = 200;
				$aff['message'] = "Manuel inséré avec succès.";

			} else {
				$aff['status'] = 500 ;
				$aff['error'] = "Isbn déjà utilisé.";
			}

		} else {
			$aff['status'] = 500 ;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}

	public function modifierManuel() {
		$aff = array();
		$manuels = new \Model\Manuel();

		$app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

		if (isset($var['id']) && isset($var['titre']) && 
			isset($var['editeur']) && isset($var['tarif']) &&
			isset($var['classe']) && isset($var['matiere']) && isset($var['annee'])) {

			$manuel = $manuels -> where('id', '=', $var['id']) -> get();
			if (! $manuel -> isEmpty()) {

				$manuel[0]->titre = htmlspecialchars($var['titre']);
				$manuel[0]->editeur = htmlspecialchars($var['editeur']);
				$manuel[0]->classe = $var['classe'];
				$manuel[0]->matiere = $var['matiere'];
				$manuel[0]->annee = $var['annee'];

				if ($manuel[0]->tarif != $var['tarif']) {

					$manuel[0]->tarif = $var['tarif'];
					$manuel[0]->save();					

					$etats = new \Model\Etat();
					$alletats = $etats -> all();

					foreach ($alletats as $etat) {
						$allprix = new \Model\Prix();
						$prix = $allprix -> where('id_manuel', '=', $manuel[0]->id)
										 -> where('id_etat', '=', $etat->id) 
										 -> get();
										 
						if (! $prix -> isEmpty()) {
							$prix[0]->montant = $manuel[0]->tarif * ($etat->pourcentage/100);							
							$prix[0]->save();
						}
					}

					$aff['status'] = 200;
					$aff['message'] = "Manuel modifié avec succès.";

				} else {
					$manuel[0]->tarif = $var['tarif'];
					$manuel[0]->save();

					$aff['status'] = 200;
					$aff['message'] = "Manuel modifié avec succès.";
				}

			} else {
				$aff['status'] = 500;
				$aff['error'] = "Manuel inexistant.";
			}

		} else {
			$aff['status'] = 500;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}

	public function exporterManuel() {
		$error = true;
		$message = "";

		$manuels = new \Model\Manuel() ;
        $allmanuels = $manuels->All();        

        if (! $allmanuels -> isEmpty()) {
            $file = fopen('manuels.csv', 'w+');
            
            foreach($allmanuels as $manuel) {
                $array = $manuel->toArray();
                fputcsv($file, $array);
            }

            fclose($file);

            $error = false;
            $message = "Manuels sauvegardés avec succès dans le fichier manuels.csv .";

        } else {
        	$message = "Erreur : Pas de manuels à exporter.";
        }

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('validation_export.html.twig');
        $tmpl->display(array("error" => $error,
                             "message" => $message));
	}

	public function importerManuel() {
		$etats = new \Model\Etat();
        $alletats = $etats -> all();

        $row = 0;
		$error = true;
		$message = "";

		if (isset($_FILES['csv'])) {

            $mimes = array('application/vnd.ms-excel','text/csv');
            if(in_array($_FILES['csv']['type'],$mimes)){

                $file = fopen($_FILES['csv']['name'], "r") ;

                while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {

                    $num = count($data);
                    if ($num == 8) {

	                    $manuels = new \Model\Manuel();
	                    $isbnmanuel = $manuels -> where('isbn', '=', htmlspecialchars($data[1])) -> get();

	                    if ($isbnmanuel -> isEmpty()) {

	                        $newmanuel = new \Model\Manuel();           
	                        $newmanuel->isbn = htmlspecialchars($data[1]);
	                        $newmanuel->titre = htmlspecialchars($data[2]);
	                        $newmanuel->matiere = $data[3];
	                        $newmanuel->classe = $data[4];
	                        $newmanuel->tarif = $data[5];
	                        $newmanuel->editeur = htmlspecialchars($data[6]);
	                        $newmanuel->annee = $data[7];
	                        $newmanuel->save();

	                        foreach($alletats as $etat) {
								$newprix = new \Model\Prix();
		                        $newprix->montant = $newmanuel->tarif * ($etat->pourcentage/100);
		                        $newprix->id_manuel = $newmanuel->id;
		                        $newprix->id_etat = $etat->id;
		                        $newprix->save();
	                        }

	                        $error = false;
	                        $row++;
	                        
	                    }
	                }

                }

                if (! $error) {
                	$message = $row." manuel(s) importé(s) avec succès.";
                } else {
                	$message = "Aucun manuels à importer.";
                }

                fclose($file);

            } else {
            	$message = "Erreur : Fichier au mauvais format.";
            }

        } else {
        	$message = "Erreur : Pas de fichier.";
        }

        $loader = new \Twig_Loader_Filesystem('Template');
        $twig = new \Twig_Environment($loader, array('debug' => true));
        $tmpl = $twig->loadTemplate('validation_import.html.twig');
        $tmpl->display(array("error" => $error,
                             "message" => $message));
	}
}