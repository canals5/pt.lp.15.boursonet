<?php
namespace Controller;
class ModePaiementController {
	public function listModePaiement(){
		$app = \Slim\Slim::getInstance();
		$aff = array();
		$mpaie = array();

		$mode_vide = true;

		$mode_paiement = new \Model\ModePaiement();
		$mode_paie=$mode_paiement->all();
		if(!$mode_paie->isEmpty()){
			$aff['status']=200;
			$mode_vide=false;
			foreach($mode_paie as $mp){
				$mpaie[] = array( 'id' => $mp->id,'libelle' => $mp->libelle);
			}
			$aff['modes'] = $mpaie;
		}

		if($mode_vide){
			$aff['status'] = 500;
			$aff['error'] = 'Aucun mode de paiement dans la base.';
		}

		echo json_encode($aff);
	}

	public function detailModePaiement($id){
		$app = \Slim\Slim::getInstance();
		$aff = array();

		$mode_vide = true;

		$mode_paiement = new \Model\ModePaiement();
		$mode_paie=$mode_paiement->where('id','=',$id)->get();
		if(!$mode_paie->isEmpty()){
			$aff['status']=200;
			$mode_vide=false;
			$aff['mode paiement'] = array( 'id' => $mode_paie[0]->id,'libelle' => $mode_paie[0]->libelle, 'caution' => $mode_paie[0]->caution);
		}

		if($mode_vide){
			$aff['status']=500;
			$aff['error']='Mauvais id ou id inexistant ';
		}

		echo json_encode($aff);
	}
	public function ajouterModePaiement() {
		$aff = array() ;
		$mode_p = new \Model\ModePaiement() ;

		if (isset($_POST['libelle']) && isset($_POST['id'])) {
			$m_p = $mode_p->where('id','=',$_POST['id'])->get();
			if($m_p->isEmpty()){
				$mode_p->id = $_POST['id'];
				$mode_p->libelle = $_POST['libelle'];
				$mode_p->save() ;
				$mode_p['status'] = 200;
				$aff['message'] = "Mode de paiement ajouté avec succès.";
			}else{
				$aff['status'] = 500 ;
				$aff['error'] = "Cet ID existe déjà.";
			}
		} else {
			$aff['status'] = 500 ;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}
	public function modifierModePaiement() {
		$aff = array() ;
		$mode_paiements = new \Model\ModePaiement() ;

		$app = \Slim\Slim::getInstance(); 
        $request = $app -> request() ;
        $var = $request -> put() ;

		if (isset($var['id']) && isset($var['libelle'])) {

			$mode_paiement = $mode_paiements -> where('id', '=', $var['id']) -> get() ;
			if (! $mode_paiement -> isEmpty()) {
				$mode_paiement[0]->libelle = $var['libelle'];
				$mode_paiement[0]->save() ;
				$aff['status'] = 200;
				$aff['message'] = "Mode de paiement modifié avec succès.";

			} else {
				$aff['status'] = 500 ;
				$aff['error'] = "Mode de paiement introuvable.";
			}
		} else {
			$aff['status'] = 500 ;
			$aff['error'] = "Données manquantes.";
		}

		echo json_encode($aff);
	}

	public function supprimerModePaiement($id){
		$aff = array();
		$mode_paiements = new \Model\ModePaiement();
		$mode_paiement = $mode_paiements->where('id','=',$id)->get();
		if(!$mode_paiement->isEmpty()){
			$mode_paiement[0]->delete();
			$aff['status'] = 200;
			$aff['message'] = 'Mode paiement supprimé avec succés.';
		}else{
			$aff['status'] = 500;
			$aff['message'] = 'Erreur dans la suppression.';
		}
		echo json_encode($aff);
	}
}