<?php
namespace Controller;
class PaiementController {
    public function listPaiement(){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $paiement_vide = true;

        $paiement = new \Model\Paiement();
        $paie=$paiement->all();
        if(!$paie->isEmpty()){
            $aff['status']=200;
            $paiement_vide=false;
            $i =0;
            foreach($paie as $p){
                $mode_p  = new \Model\ModePaiement();
                $mp = $mode_p->where('id','=',$p->id_mode_paiement)->get();
                $aff['paiement '.$i] = array( 'id' => $p->id,'id_achat' => $p->id_achat, 'montant' => $p->montant, 'date' =>$p->date,'mode paiement' =>array('id' => $mp[0]->id , 'libelle' => $mp->libelle, 'caution' => $mp->caution) );
                $i++;
            }
        }

        if($paiement_vide){
            $aff['status']=500;
            $aff['error']='Aucun paiement dans la base.';
        }

        echo json_encode($aff);
    }

    public function detailPaiement($id){
        $app = \Slim\Slim::getInstance();
        $aff = array();

        $paiement_vide = true;

        $paiement = new \Model\Paiement();
        $paie=$paiement->where('id','=',$id)->get();
        if(!$paie->isEmpty()){
            $aff['status']=200;
            $paiement_vide=false;
            $mode_p  = new \Model\ModePaiement();
            $mp = $mode_p->where('id','=',$paie[0]->id_mode_paiement)->get();
            $aff['paiement'] = array( 'id' => $paie[0]->id,'id_achat' => $paie[0]->id_achat, 'montant' => $paie[0]->montant, 'date' =>$paie[0]->date,'mode paiement' =>array('id' => $mp[0]->id , 'libelle' => $mp[0]->libelle, 'caution' => $mp[0]->caution) );
        }

        if($paiement_vide){
            $aff['status']=500;
            $aff['error']='id incorrect ou inexistant.';
        }

        echo json_encode($aff);
    }

    public function listePaiementFamille($id)
    {
        $aff = array();
        $paiement = new \Model\Paiement();
        $paie = $paiement->where('id_achat', '=', $id)->get();
        if (!$paie->isEmpty()) {
            $aff['status'] = 200;
            $array = array();
            foreach ($paie as $p) {
                $mode_p  = new \Model\ModePaiement();
                $mp = $mode_p->where('id','=',$p->id_mode_paiement)->get();
                $array[] = array('id' => $p->id, 'id_achat' => $p->id_achat, 'montant' => $p->montant, 'date' =>$p->date,'mode' =>array('id' => $mp[0]->id , 'libelle' => $mp[0]->libelle));
            }
            $aff['paiements'] = $array;
        } else {
            $aff['status'] = 500;
            $aff['error'] = 'Aucun paiement pour cette famille.';
        }

        echo json_encode($aff);
    }
}