<?php
namespace Controller;
class RecepisseController {
    private $header = 'Bourse aux livres 2015';
    private $footer = "La bourse aux livres FCPE est organisée par des parents d'élèves bénévoles. Si vous constatez une erreur, merci de vous adresser aux organisateurs.";
    public function depot($id)
    {
        $famille = new \Model\Famille();
        $depot = new \Model\Depot();
        $exemplaire = new \Model\Exemplaire();
        $manuel = new \Model\Manuel();
        $etat = new \Model\Etat();
        $enfant = new \Model\Enfant();
        $fam = $famille->where('id', '=', $id)->get();

        $ok_recepisse = true;
        if (!$fam->isEmpty()) {
            $exe = $exemplaire->where('id_famille_depot', '=', $id)->get();
            if (!$exe->isEmpty()) {
                $i = 0;
                $depots = array();
                foreach ($exe as $e) {
                    $i++;
                    $man = $manuel->where('isbn', '=', $e->id_manuel)->get();
                    $eta = $etat->where('id', '=', $e->id_etat)->get();
                    $actuel = "En stock";
                    if ($e->id_famille_achat != null) {
                        if ($e->id_famille_achat == $e->id_famille_depot) {
                            $actuel = "Rendu";
                        } else {
                            $actuel = "Vendu";
                        }
                    }
                    $enfant_dep = "-";
                    if ($e->id_enfant_depot != null) {
                        $enf = $enfant->where('id', '=', $e->id_enfant_depot)->get();
                        $enfant_dep = $enf[0]->prenom;
                    } 
                    $depots[] = array('id' => $e->id, 'titre' => $man[0]->titre, 'etat' => $eta[0]->libelle, 'actuel' => $actuel, 'enfant' => $enfant_dep);
                }
            } else {
                $ok_recepisse = false;
            }
        } else {
            $ok_recepisse = false;
        }

        if ($ok_recepisse) {
            $loader = new \Twig_Loader_Filesystem('Template');
            $twig = new \Twig_Environment($loader, array('debug' => true));
            $tmpl = $twig->loadTemplate('recepisse_depot.html.twig');
            $tmpl->display(array('header' => $this->header, 'footer' => $this->footer, 'famille' => $fam[0], 'depots' => $depots, 'nb_livre' => $i));
        } else {
            $loader = new \Twig_Loader_Filesystem('Template');
            $twig = new \Twig_Environment($loader, array('debug' => true));
            $tmpl = $twig->loadTemplate('recepisse_depot.html.twig');
            $tmpl->display(array('error' => "Aucun récépissé à afficher pour cette famille."));
        }
    }

    public function achat($id)
    {
        $famille = new \Model\Famille();
        $achat = new \Model\Achat();
        $exemplaire = new \Model\Exemplaire();
        $manuel = new \Model\Manuel();
        $etat = new \Model\Etat();
        $enfant = new \Model\Enfant();
        $paiement = new \Model\Paiement();
        $fam = $famille->where('id', '=', $id)->get();

        $ok_recepisse = true;
        if (!$fam->isEmpty()) {
            $exe = $exemplaire->where('id_famille_achat', '=', $id)->get();
            if (!$exe->isEmpty()) {
                $i = 0;
                $achats = array();
                foreach ($exe as $e) {
                    $i++;
                    $man = $manuel->where('isbn', '=', $e->id_manuel)->get();
                    $eta = $etat->where('id', '=', $e->id_etat)->get();
                    $enfant_ach = "-";
                    if ($e->id_enfant_achat != null) {
                        $enf = $enfant->where('id', '=', $e->id_enfant_achat)->get();
                        $enfant_ach = $enf[0]->prenom;
                    }
                    $achats[] = array('id' => $e->id, 'titre' => $man[0]->titre, 'etat' => $eta[0]->libelle, 'enfant' => $enfant_ach);
                }
            } else {
                $ok_recepisse = false;
            }
            $ach = $achat->where('id_famille','=',$id)->get();
            if (!$ach->isEmpty()) {
                $paie = $paiement->where('id_achat', '=', $id)->get();
                $total_paiement = 0;
                foreach ($paie as $pai) {
                    $total_paiement += $pai->montant;
                }
                $montants = array('total' => $ach[0]->montant ,'frais' => $ach[0]->frais, 'restant' => $ach[0]->solde + $ach[0]->frais, 'paiement' => $total_paiement);
            } else {
                $ok_recepisse = false;
            }
        } else {
            $ok_recepisse = false;
        }

        if ($ok_recepisse) {
            $loader = new \Twig_Loader_Filesystem('Template');
            $twig = new \Twig_Environment($loader, array('debug' => true));
            $tmpl = $twig->loadTemplate('recepisse_achat.html.twig');
            $tmpl->display(array('header' => $this->header, 'footer' => $this->footer, 'famille' => $fam[0], 'achats' => $achats, 'nb_livre' => $i, 'montants' => $montants));
        } else {
            $loader = new \Twig_Loader_Filesystem('Template');
            $twig = new \Twig_Environment($loader, array('debug' => true));
            $tmpl = $twig->loadTemplate('recepisse_achat.html.twig');
            $tmpl->display(array('error' => "Aucun récépissé à afficher pour cette famille."));
        }
    }

}