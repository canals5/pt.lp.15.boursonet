<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Achat extends Eloquent {
    protected $table = 'achat';
    protected $primaryKey = 'id';
    public $timestamps=false;

        public function exemplaire()
    {
    	return $this->belongsTo('Model\Exemplaire', 'id_exemplaire');
    }

        public function famille()
    {
    	return $this->belongsTo('Model\Famille', 'id_famille');
    }

        public function paiement()
    {
    	return $this->belongsTo('Model\Paiement', 'id_paiement');
    }
}