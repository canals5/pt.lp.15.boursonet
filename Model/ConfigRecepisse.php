<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ConfigRecepisse extends Eloquent {
    protected $table = 'config_recepisse';
    public $timestamps=false;
}