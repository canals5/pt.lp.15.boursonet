<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Depot extends Eloquent {
    protected $table = 'depot';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function exemplaire()
    {
    	return $this->belongsTo('Model\Exemplaire', 'id_exemplaire');
    }

    public function famille()
    {
    	return $this->belongsTo('Model\Famille', 'id_famille');
    }
}