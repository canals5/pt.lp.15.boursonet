<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Enfant extends Eloquent {
    protected $table = 'enfant';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function famille()
    {
    	return $this->belongsTo('Model\Famille', 'id_famille');
    }
}