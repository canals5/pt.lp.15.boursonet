<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etat extends Eloquent {
    protected $table = 'etat';
    protected $primaryKey = 'id';
    public $timestamps=false;
}