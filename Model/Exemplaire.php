<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Exemplaire extends Eloquent {
    protected $table = 'exemplaire';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function manuel()
    {
    	return $this->belongsTo('Model\Manuel', 'id_manuel');
    }
}