<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Famille extends Eloquent {
    protected $table = 'famille';
    protected $primaryKey = 'id';
    public $timestamps=false;
}