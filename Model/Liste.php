<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Liste extends Eloquent {
    protected $table = 'liste';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function manuel()
    {
    	return $this->belongsTo('Model\Manuel', 'id_manuel');
    }
}