<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ModePaiement extends Eloquent {
    protected $table = 'mode_paiement';
    protected $primaryKey = 'id';
    public $timestamps=false;
}