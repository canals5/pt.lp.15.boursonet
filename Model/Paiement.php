<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Paiement extends Eloquent {
    protected $table = 'paiement';
    protected $primaryKey = 'id';
    public $timestamps=false;

    public function mode_paiement()
    {
    	return $this->belongsTo('Model\ModePaiement', 'id_mode_paiement');
    }
}