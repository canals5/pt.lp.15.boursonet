<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Role extends Eloquent {
    protected $table = 'role';
    protected $primaryKey = 'id';
    public $timestamps=false;
}