<?php
namespace Model;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Taux extends Eloquent {
    protected $table = 'taux';
    public $timestamps=false;
}