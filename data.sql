INSERT INTO `role` (`id`, `libelle`) VALUES
(1, 'Bénévole'),
(10, 'Administrateur'),
(100, 'Super-Administrateur');

INSERT INTO `user` (`id`, `login`, `pass`, `id_role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 100);