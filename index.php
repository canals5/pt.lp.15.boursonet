<?php
session_start();
require 'vendor/autoload.php';
\Model\ConnexionDB::createDB(parse_ini_file('config.ini'));
$app = new \Slim\Slim();

if (isset($_SESSION['logged'])) {
    $app->get(
        '/',
        function () {
            $index = new \Controller\IndexController();
            $index->index();
        });
    $app->get(
        '/export/famille',
        function () {
            $famille = new \Controller\FamilleController();
            $famille->exporterFamille();
        });
    $app->post(
        '/import/famille',
        function () {
            $famille = new \Controller\FamilleController();
            $famille->importerFamille();
        });
    $app->get(
        '/export/manuel',
        function () {
            $manuel = new \Controller\ManuelController();
            $manuel->exporterManuel();
        });
    $app->post(
        '/import/manuel',
        function () {
            $manuel = new \Controller\ManuelController();
            $manuel->importerManuel();
        });
    $app->get(
        '/famille/:id',
        function ($id) {
            $index = new \Controller\IndexController();
            $index->bourse($id);
        });
    $app->get(
        '/famille/:id_famille/enfant/:id_enfant',
        function ($id_famille, $id_enfant) {
            $index = new \Controller\IndexController();
            $index->enfant($id_famille, $id_enfant);
        });
    $app->get(
        '/manuel/:id',
        function ($id) {
            $manuel = new \Controller\ManuelController();
            $manuel->manuel($id);
        });
    $app->get(
        '/recepisse/depot/:id',
        function ($id) {
            $recepisse = new \Controller\RecepisseController();
            $recepisse->depot($id);
        });
    $app->get(
        '/recepisse/achat/:id',
        function ($id) {
            $recepisse = new \Controller\RecepisseController();
            $recepisse->achat($id);
        });
    $app->get(
        '/admin',
        function () {
            $admin = new \Controller\AdminController();
            $admin->index();
        });
    $app->get(
        '/compte',
        function () {
            $user = new \Controller\UserController();
            $user->index();
        });
    $app->get(
        '/logout',
        function () {
            $user = new \Controller\UserController();
            $user->logout();
        });

    $app->group(
        '/api',
        function () use ($app) {

            $tab = new \Controller\AdminController();
            $app->get("/stats", function() use ($tab, $app) 
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $tab->tableauDeBord();
            })->name("tableauDeBord");
            $fam = new \Controller\FamilleController();
            $app->get("/familles", function() use($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->listFamilles();
            })->name("familles");
            $app->get("/familles/:id", function($id) use($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->detailFamille($id);
            })->name("famille");
            $app->post("/familles", function () use ($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->verifAjoutFamille();
            })->name("ajoutFamille");
            $app->put("/familles", function () use ($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->modifierFamille();
            })->name("modifFamille");
            $app->get("/nom/familles", function () use ($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->nomFamille();
            })->name("nomFamille");
            $app->post("/recherche/familles", function () use ($fam, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $fam->rechercheFamille();
            })->name("rechercheFamille");

            $achat = new \Controller\AchatController();
            $app->get("/achats", function() use($achat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $achat->listAchats();
            })->name("achats");
            $app->get("/achats/:id", function($id) use($achat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $achat->detailAchat($id);
            })->name("achat");
            $app->get("/achats/familles/:id", function($id) use($achat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $achat->listAchatFamille($id);
            })->name("achatFamille");
            $app->post("/achats", function() use($achat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $achat->ajoutAchat();
            })->name("AjouterAchat");
            $app->post("/achat/paiement", function () use($achat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $achat->paiementAchat();
            })->name("paiementAchat");


            $depot = new \Controller\DepotController();
            $app->get("/depots", function() use($depot, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $depot->listDepots();
            })->name("depots");
            $app->get("/depots/:id", function($id) use($depot, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $depot->detailDepot($id);
            })->name("depot");
            $app->get("/depots/familles/:id", function($id) use($depot, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $depot->listDepotsFamille($id);
            })->name("depotFamille");
            $app->post("/depot/frais_envoi", function () use($depot, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $depot->appliquerFraisEnvoi();
            })->name("appliquerFrais");
            $app->post("/depot/paiement", function () use($depot, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $depot->paiementDepot();
            })->name("paiementDepot");

            $enfant = new \Controller\EnfantController();
            $app->get("/enfants", function() use($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->listEnfants();
            })->name("enfants");
            $app->post("/enfants", function() use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->verifAjoutEnfant();
            })->name("ajoutEnfant");
            $app->put("/enfants", function() use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->modifierEnfant();
            })->name("modifEnfant");
            $app->get("/enfants/:id", function($id) use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->detailEnfant($id);
            })->name("enfant");
            $app->get("/familles/:id/enfants", function($id) use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->listEnfantsFamille($id);
            })->name("enfantsFamille");
            $app->get("/familles/:id/enfants/nombre", function($id) use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->nombreEnfants($id);
            })->name("nombreEnfants");
            $app->delete("/familles/:id_fam/enfants/:id_enf", function($id_fam,$id_enf) use ($enfant, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $enfant->supprimerEnfants($id_fam,$id_enf);
            })->name("deleteEnfants");

            $etat = new \Controller\EtatController();
            $app->get("/etats", function() use($etat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $etat->listEtat();
            })->name("etats");
            $app->post("/etats", function() use($etat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $etat->ajouterEtat();
            })->name("ajoutEtat");
            $app->put("/etats", function() use($etat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $etat->modifierEtat();
            })->name("modifEtat");
            $app->get("/etats/:id", function($id) use($etat, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $etat->detailEtat($id);
            })->name("etat");

            $exemp = new \Controller\ExemplaireController();
            $app->get("/exemplaires", function() use($exemp, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $exemp->listExemplaire();
            })->name("exemplaires");
            $app->post("/exemplaires", function() use($exemp, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $exemp->ajoutExemplaire();
            })->name("ajoutExemplaire");
            $app->get("/exemplaires/:id", function($id) use($exemp, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $exemp->detailExemplaire($id);
            })->name("exemplaire");
            $app->delete("/exemplaires", function() use ($exemp, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $exemp->supprimerExemplaire();
            })->name("deleteExemplaire");

            $user = new \Controller\UserController();
            $app->post("/users/ajouter", function () use ($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->ajoutCompte();
            })->name("inscription");
            $app->get("/users", function() use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->listUser();
            })->name("users");
            $app->get("/users/:id", function($id) use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->detailUser($id);
            })->name("user");
            $app->get("/benevole/users/:id", function($id) use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->isBenevole($id);
            })->name("userBenvole");
            $app->get("/benevole/users/", function() use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->listBenevole();
            })->name("listBenvole");
            $app->put("/user/role/", function() use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->modifRole();
            })->name("modifRoleUser");
            $app->put("/user/pass/", function() use($user, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $user->modifPassword();
            })->name("modifRolePassword");



            $manuel = new \Controller\ManuelController() ;
            $app->get("/manuels", function() use ($manuel, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $manuel -> infosListeManuels() ;
            })->name("manuels");

            $app->post("/manuels", function() use ($manuel, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $manuel -> ajoutManuel() ;
            })->name("ajoutManuel");

            $app->put("/manuels", function() use ($manuel, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $manuel -> modifierManuel() ;
            })->name("modifManuel");

            $app->get("/manuels/:id", function($id) use ($manuel, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $manuel -> infosManuel($id) ;
            })->name("manuel");
            $app->post("/recherche/manuels", function () use ($manuel, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $manuel->rechercheManuel();
            })->name("rechercheManuel");

            $liste = new \Controller\ListController() ;
            $app->get("/listes", function() use ($liste, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $liste -> listListeManuel() ;
            })->name("listes");

            $app->get("/listes/:id", function($id) use ($liste, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $liste -> detailListeManuel($id) ;
            })->name("liste");

             $mode_paie = new \Controller\ModePaiementController() ;
            $app->get("/mode_paiements", function() use ($mode_paie, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $mode_paie -> listModePaiement() ;
            })->name("mode_paiements");

            $app->get("/mode_paiements/:id", function($id) use ($mode_paie, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $mode_paie -> detailModePaiement($id) ;
            })->name("mode_paiement");
            $app->post("/mode_paiement", function() use($mode_paie, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $mode_paie->ajouterModePaiement();
            })->name("ajoutModePaiement");
            $app->put("/mode_paiement", function() use($mode_paie, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $mode_paie->modifierModePaiement();
            })->name("modifModePaiement");
            $app->delete("/mode_paiement/:id", function($id) use($mode_paie, $app)
            {
                $app->response->headers->set('Content-Type', 'application/json');
                $mode_paie->supprimerModePaiement($id);
            })->name("supprimerModePaiement");

            $paie = new \Controller\PaiementController() ;
            $app->get("/paiements", function() use ($paie, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $paie -> listPaiement() ;
            })->name("paiements");
            $app->get("/paiements/:id", function($id) use ($paie, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $paie -> detailPaiement($id) ;
            })->name("paiement");
            $app->get("/paiements/famille/:id", function($id) use ($paie, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $paie -> listePaiementFamille($id) ;
            })->name("paiementFamille");

            $prix = new \Controller\PrixController() ;
            $app->get("/prix", function() use ($prix, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $prix -> listPrix() ;
            })->name("prix");

            $role = new \Controller\RoleController() ;
            $app->get("/roles", function() use ($role, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $role -> listRole() ;
            })->name("roles");

            $taux = new \Controller\TauxController();
            $app->get("/taux", function() use ($taux, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $taux->listeTaux();
            })->name("taux");
            $app->post("/taux", function() use($taux, $app) {
                $app->response->headers->set('Content-Type', 'application/json');
                $taux->modifTaux();
            })->name("modifTaux");

        });
} else {
    $app->get(':url', function($url) use ($app) {
        $user = new \Controller\UserController();
        $user->login();
    })->conditions(array('url' => '.+'));
    $app->post(':url', function($url) use ($app) {
        $user = new \Controller\UserController();
        $user->verifLogin();
    })->conditions(array('url' => '.+'));
}
$app->notFound(function () {
    $index = new \Controller\IndexController();
    $index->notFound();
});
$app->run();