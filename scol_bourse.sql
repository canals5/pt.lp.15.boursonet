﻿-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 25 Mars 2015 à 11:12
-- Version du serveur :  5.6.20
-- Version de PHP :  5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `scol_bourse`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

CREATE TABLE IF NOT EXISTS `achat` (
`id` int(11) NOT NULL,
  `id_famille` int(11) NOT NULL,
  `date_creation` date NOT NULL,
  `date_modif` date NOT NULL,
  `frais` decimal(10,2) NOT NULL DEFAULT '0.00',
  `montant` decimal(10,2) NOT NULL DEFAULT '0.00',
  `solde` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `config_recepisse`
--

CREATE TABLE IF NOT EXISTS `config_recepisse` (
  `header` text CHARACTER SET latin1 NOT NULL,
  `footer` text CHARACTER SET latin1 NOT NULL,
  `logo` text CHARACTER SET latin1 NOT NULL,
  `text` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `depot`
--

CREATE TABLE IF NOT EXISTS `depot` (
`id` int(11) NOT NULL,
  `id_famille` int(11) NOT NULL,
  `date_cree` date NOT NULL,
  `date_dernier_ajout` date NOT NULL,
  `frais` decimal(10,2) NOT NULL DEFAULT '0.00',
  `montant` decimal(10,2) NOT NULL DEFAULT '0.00',
  `montant_vendu` decimal(10,2) NOT NULL DEFAULT '0.00',
  `montant_paye` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frais_envoi` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `enfant`
--

CREATE TABLE IF NOT EXISTS `enfant` (
`id` int(11) NOT NULL,
  `nom` text CHARACTER SET latin1 NOT NULL,
  `prenom` text CHARACTER SET latin1 NOT NULL,
  `classe` text CHARACTER SET latin1 NOT NULL,
  `id_famille` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE IF NOT EXISTS `etat` (
  `id` int(11) NOT NULL,
  `libelle` text CHARACTER SET latin1 NOT NULL,
  `pourcentage` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `exemplaire`
--

CREATE TABLE IF NOT EXISTS `exemplaire` (
  `id` varchar(20) NOT NULL,
  `id_manuel` varchar(20) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `id_famille_depot` int(11) NOT NULL,
  `id_famille_achat` int(11) DEFAULT NULL,
  `id_enfant_depot` int(11) DEFAULT NULL,
  `id_enfant_achat` int(11) DEFAULT NULL,
  `date_depot` date NOT NULL,
  `date_achat` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

CREATE TABLE IF NOT EXISTS `famille` (
`id` int(11) NOT NULL,
  `nom` text CHARACTER SET latin1 NOT NULL,
  `prenom_resp` text CHARACTER SET latin1 NOT NULL,
  `adresse` text CHARACTER SET latin1 NOT NULL,
  `complement` text CHARACTER SET latin1,
  `cp` int(11) NOT NULL,
  `ville` text CHARACTER SET latin1 NOT NULL,
  `tel` text CHARACTER SET latin1 NOT NULL,
  `mail` varchar(255) CHARACTER SET latin1 NOT NULL,
  `notes` text CHARACTER SET latin1 NOT NULL,
  `adherent` tinyint(1) NOT NULL,
  `frais` tinyint(1) NOT NULL,
  `adresse_fact` text CHARACTER SET latin1 NOT NULL,
  `complement_fact` text CHARACTER SET latin1,
  `cp_fact` int(11) NOT NULL,
  `ville_fact` text CHARACTER SET latin1 NOT NULL,
  `nom_fact` text CHARACTER SET latin1 NOT NULL,
  `prenom_fact` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE IF NOT EXISTS `liste` (
`id` int(11) NOT NULL,
  `nom` text CHARACTER SET latin1 NOT NULL,
  `classe` text CHARACTER SET latin1 NOT NULL,
  `id_manuel` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `manuel`
--

CREATE TABLE IF NOT EXISTS `manuel` (
`id` int(11) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `titre` text CHARACTER SET latin1 NOT NULL,
  `matiere` varchar(255) NOT NULL,
  `classe` text CHARACTER SET latin1 NOT NULL,
  `tarif` decimal(10,2) NOT NULL,
  `editeur` text CHARACTER SET latin1 NOT NULL,
  `annee` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5661 ;

-- --------------------------------------------------------

--
-- Structure de la table `mode_paiement`
--

CREATE TABLE IF NOT EXISTS `mode_paiement` (
  `id` int(11) NOT NULL,
  `libelle` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

CREATE TABLE IF NOT EXISTS `paiement` (
`id` int(11) NOT NULL,
  `id_achat` int(11) NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `id_mode_paiement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `prix`
--

CREATE TABLE IF NOT EXISTS `prix` (
`id` int(11) NOT NULL,
  `id_manuel` int(11) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `montant` decimal(10,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `libelle` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Structure de la table `taux`
--

CREATE TABLE IF NOT EXISTS `taux` (
`id` int(11) NOT NULL,
  `frais_dossier` decimal(10,2) NOT NULL,
  `frais_envoi` decimal(10,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `login` text CHARACTER SET latin1 NOT NULL,
  `pass` text CHARACTER SET latin1 NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `achat`
--
ALTER TABLE `achat`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `depot`
--
ALTER TABLE `depot`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `enfant`
--
ALTER TABLE `enfant`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `exemplaire`
--
ALTER TABLE `exemplaire`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `famille`
--
ALTER TABLE `famille`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `liste`
--
ALTER TABLE `liste`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `manuel`
--
ALTER TABLE `manuel`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mode_paiement`
--
ALTER TABLE `mode_paiement`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `paiement`
--
ALTER TABLE `paiement`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prix`
--
ALTER TABLE `prix`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `taux`
--
ALTER TABLE `taux`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `achat`
--
ALTER TABLE `achat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `depot`
--
ALTER TABLE `depot`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `enfant`
--
ALTER TABLE `enfant`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `famille`
--
ALTER TABLE `famille`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `liste`
--
ALTER TABLE `liste`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `manuel`
--
ALTER TABLE `manuel`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5661;
--
-- AUTO_INCREMENT pour la table `paiement`
--
ALTER TABLE `paiement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `prix`
--
ALTER TABLE `prix`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT pour la table `taux`
--
ALTER TABLE `taux`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
