$(document).ready(function () {
    $('.menu .administration').addClass("active");
    $('.menu .item').tab();
    $('input[type=file]').bootstrapFileInput();

    $('#modal_succes').modal({
        onHide : function () {
            location.reload(true);
        }
    });

    //dashboard
    $.ajax({
        type: "GET",
        url: "/api/stats"
    }).done(function (msg) {
        $('#exemplaires_deposes').text(msg.exemplaires);
        $('#exemplaires_vendus').text(msg.exemplaires_vendus);
        $('#exemplaires_rendus').text(msg.exemplaires_rendus);
        $('#exemplaires_stock').text(msg.exemplaires - msg.exemplaires_vendus - msg.exemplaires_rendus);
        $('#nb_familles').text(msg.familles);
        $('#nb_familles_depot').text(msg.familles_depots);
        $('#nb_familles_achat').text(msg.familles_achats);
        $('#total_livres_deposes').text(msg.total_depots + " €");
        $('#total_livres_vendus').text(msg.total_vendus + " €");
        $('#total_paiements').text(msg.total_paiement + " €");
    });
    
    // benevoles
    $.ajax({
        type: "GET",
        url: "/api/benevole/users/"
    }).done(function (msg) {
        $('.select_supprimer_benevole').select2({
            placeholder: "Chercher un bénévole",
            allowClear: true,
            data: msg
        });
    });

    $('#ajout_benevole_form').submit(function (e){
        e.preventDefault();
        $(this).addClass('loading');
        var login = $('#ajout_benevole_utilisateur').val();
        var password = $('#ajout_benevole_password').val();
        $.ajax({
            type: "POST",
            url: "/api/users/ajouter",
            data: {login: login, password: password}
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#ajout_benevole_form').html("<div class='ui success message' style='display:block'><div class='header'>Bénévole créé</div><p>Identifiant : " + msg.login + "<br />Mot de passe : " + msg.password + "</p></div><div id='ajouter_autre_benevole' class='ui button'>Ajouter un autre bénévole</div>");
                $('#ajout_benevole_form').removeClass('loading');
                $('#ajouter_autre_benevole').on('click', function (e) {
                    e.preventDefault();

                    $('#ajout_benevole_form').html("<div class='field'><label>Utilisateur</label><input placeholder='Utilisateur' type='text'  id='ajout_benevole_utilisateur' required></div><div class='field'><label>Mot de passe</label><input placeholder='Mot de passe' type='password' id='ajout_benevole_password' required></div><button class='ui submit button'>Créer le compte</button>");
                });
            } else {
                $('#modal_erreur .content').html(msg.error);
                $('#modal_erreur').modal('show');
            }
        });
        $(this).removeClass('loading');
    });

    $('#supprimer_benevole_form').submit(function (e) {
        e.preventDefault();
    });

    //manuels
    $('#button_rechercher_manuel').on('click', function (e) {
        e.preventDefault();
        if (($('#rechercher_manuel_isbn').val() == "") && ($('#rechercher_manuel_titre').val() == "")) {
            $('#modal_erreur .content').html('Veuillez compléter au moins un champ du formulaire de recherche.');
            $('#modal_erreur').modal('show');
        } else {
            $.ajax({
                type: "POST",
                url: "/api/recherche/manuels",
                data: {isbn: $('#rechercher_manuel_isbn').val(), titre: $('#rechercher_manuel_titre').val()}
            }).done(function (msg) {
                if (msg.status == 200) {
                    var content = "<table class='ui striped table'><thead><tr><th>Code manuel</th><th>Titre</th><th>Editeur</th><th>Classe</th><th>Tarif neuf</th></tr></thead><tbody>";
                    msg.manuel.forEach(function (m) {
                        content += "<tr data-id='" + m.id + "' class='select_manuel'><td>" + m.isbn + "</td><td>" + m.titre + "</td><td>" + m.editeur + "</td><td>" + m.classe + "</td><td>" + m.tarif + " €</td></tr>";
                    });
                    content += "</tbody></table>";
                    $('#recherche_manuel .content').html(content);
                } else {
                    $('#recherche_manuel .content').html("<div class='not_found'>" + msg.error + "</div>");
                }
                $('#recherche_manuel').css('display', '');
                $('.select_manuel').on('click', function (e) {
                    if($(this).data('id') !== undefined){
                        document.location = "/manuel/" + $(this).data('id');
                    }
                });
            });
        }
    });
    $('#button_ajouter_manuel').on('click', function (e) {
        e.preventDefault();
        $('#ajouter_manuel').css('display', '');
        $('#button_ajouter_manuel').css('display', 'none');
        $('#ajouter_manuel_isbn').focus();
    });
    $('#annuler_ajouter_manuel').on('click', function (e) {
        e.preventDefault();
        $('#ajouter_manuel').css('display', 'none');
        $('#button_ajouter_manuel').css('display', '');
        $('#form_ajouter_manuel')[0].reset();
        $('.field').removeClass('error');
    });
    $('#form_ajouter_manuel').submit(function (e) {
        e.preventDefault();
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var isbn = $('#ajouter_manuel_isbn').val();
        var titre = $('#ajouter_manuel_titre').val();
        var matiere = $('#ajouter_manuel_matiere').val();
        var classe = $('#ajouter_manuel_classe').val();
        var editeur = $('#ajouter_manuel_editeur').val();
        var annee = $('#ajouter_manuel_annee').val();
        var tarif = $('#ajouter_manuel_tarif').val();
        if (isbn == "") {
            ok_ajout = false;
            $('#ajouter_manuel_isbn').parent().addClass('error');
            message_erreur += "<li>Code manuel</li>";
        }
        if (titre == "") {
            ok_ajout = false;
            $('#ajouter_manuel_titre').parent().addClass('error');
            message_erreur += "<li>Titre</li>";
        }
        if (matiere == "") {
            ok_ajout = false;
            $('#ajouter_manuel_matiere').parent().addClass('error');
            message_erreur += "<li>Matière</li>";
        }
        if (classe == "") {
            ok_ajout = false;
            $('#ajouter_manuel_classe').parent().addClass('error');
            message_erreur += "<li>Classe</li>";
        }
        if (editeur == "") {
            ok_ajout = false;
            $('#ajouter_manuel_editeur').parent().addClass('error');
            message_erreur += "<li>Editeur</li>";
        }
        if (annee == "") {
            ok_ajout = false;
            $('#ajouter_manuel_annee').parent().addClass('error');
            message_erreur += "<li>Année d'édition</li>";
        }
        if (tarif == "") {
            ok_ajout = false;
            $('#ajouter_manuel_tarif').parent().addClass('error');
            message_erreur += "<li>Nom</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/manuels",
                data: {isbn: isbn, titre: titre, matiere: matiere, classe: classe, editeur: editeur, annee: annee, tarif: tarif}
            }).done(function (msg) {
                if (msg.status == 200) {
                    $('#modal_succes .content').html(msg.message);
                    $('#modal_succes').modal('show');
                } else {
                    $('#modal_erreur .content').html(msg.error);
                    $('#modal_erreur').modal('show');
                }
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');


    });

    //frais
    $.ajax({
        type: "GET",
        url: "/api/taux"
    }).done(function (msg) {
        if (msg.status == 200) {
            $('#frais_dossier').val(msg.taux_dossier);
            $('#frais_envoi').val(msg.taux_envoi);
        }
    });
    $('#form_taux').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/api/taux",
            data: {taux_dossier: $('#frais_dossier').val(), taux_envoi: $('#frais_envoi').val()}
        }).done(function (msg) {
            if (msg.status == 200) {
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            } else {
                $('#modal_erreur .content').html(msg.error);
                $('#modal_erreur').modal('show');
            }
        });
    });

    //etats
    $.ajax({
        type: "GET",
        url: "/api/etats"
    }).done(function (msg) {
        if (msg.status == 200) {
            var content = "<table class='ui table'><thead><tr><th>ID état</th><th>Libellé</th><th>Pourcentage</th></tr></thead><tbody>";
            msg.etats.forEach(function (e) {
                content += "<tr><td>" + e.id + "</td><td>" + e.libelle + "</td><td>" + e.pourcentage + "</td></tr>";
            });
            $('#liste_etats').html(content + "</tbody></table>");
        } else {
            $('#liste_etats').html("<div class='not_found'>" + msg.error + "</div>");
        }
    });
    $('#button_ajouter_etat').on('click', function (e) {
        e.preventDefault();
        $(this).css('display', 'none');
        $('#ajouter_etat').css('display', '');
        $('#ajouter_etat_id').focus();
    });
    $('#button_annuler_ajouter_etat').on('click', function (e) {
        e.preventDefault();
        $('#ajouter_etat').css('display', 'none');
        $('#button_ajouter_etat').css('display', '');
        $('#form_ajouter_etat')[0].reset();
        $('.field').removeClass('error');
    });
    $('#form_ajouter_etat').submit(function (e) {
        e.preventDefault();
        $('.field').removeClass('error');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var id = $('#ajouter_etat_id').val();
        var libelle = $('#ajouter_etat_libelle').val();
        var pourcentage = $('#ajouter_etat_pourcentage').val();
        if (id == "") {
            ok_ajout = false;
            $('#ajouter_etat_id').parent().addClass('error');
            message_erreur += "<li>ID</li>";
        }
        if (libelle == "") {
            ok_ajout = false;
            $('#ajouter_etat_libelle').parent().addClass('error');
            message_erreur += "<li>Libellé</li>";
        }
        if (pourcentage == "") {
            ok_ajout = false;
            $('#ajouter_etat_pourcentage').parent().addClass('error');
            message_erreur += "<li>Pourcentage</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/etats",
                data: {id : id, libelle: libelle, pourcentage: pourcentage}
            }).done(function (msg) {
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
    });


    //mode de paiment
    $.ajax({
        type: "GET",
        url: "/api/mode_paiements"
    }).done(function (msg) {
        if (msg.status == 200) {
            var content = "<table class='ui table'><thead><tr><th>ID mode</th><th>Libellé</th></tr></thead><tbody>";
            msg.modes.forEach(function (p) {
                content += "<tr><td>" + p.id + "</td><td>" + p.libelle + "</td></tr>";
            });
            $('#liste_mode_paiement').html(content + "</tbody></table>");
        } else {
            $('#liste_mode_paiement').html("<div class='not_found'>" + msg.error + "</div>");
        }
    });
    $('#button_ajouter_mode_paiement').on('click', function (e) {
        e.preventDefault();
        $(this).css('display', 'none');
        $('#ajouter_mode_paiement').css('display', '');
        $('#ajouter_mode_paiement_id').focus();
    });
    $('#button_annuler_ajouter_mode_paiement').on('click', function (e) {
        e.preventDefault();
        $('#ajouter_mode_paiement').css('display', 'none');
        $('#button_ajouter_mode_paiement').css('display', '');
        $('#form_ajouter_mode_paiement')[0].reset();
        $('.field').removeClass('error');
    });
    $('#form_ajouter_mode_paiement').submit(function (e) {
        e.preventDefault();
        $('.field').removeClass('error');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var id = $('#ajouter_mode_paiement_id').val();
        var libelle = $('#ajouter_mode_paiement_libelle').val();
        if (id == "") {
            ok_ajout = false;
            $('#ajouter_mode_paiement_id').parent().addClass('error');
            message_erreur += "<li>ID</li>";
        }
        if (libelle == "") {
            ok_ajout = false;
            $('#ajouter_mode_paiement_libelle').parent().addClass('error');
            message_erreur += "<li>Libellé</li>";
        }

        if (ok_ajout) {
            $.ajax({
                type: "POST",
                url: "/api/mode_paiement",
                data: {id : id, libelle: libelle}
            }).done(function (msg) {
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
    });
    
});