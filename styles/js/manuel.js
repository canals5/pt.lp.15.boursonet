$(document).ready(function () {

    $('#modal_succes').modal({
        onHide : function () {
            location.reload(true);
        }
    });
    $('#form_modifier_manuel').submit(function (e) {
        var id = $('#modifier_manuel').data('id');
        e.preventDefault();
        $('.field').removeClass('error');
        $(this).addClass('loading');
        var ok_ajout = true;
        var message_erreur = "<ul>";
        var tarif = $('#modifier_manuel_tarif').val();
        var titre = $('#modifier_manuel_titre').val();
        var matiere = $('#modifier_manuel_matiere').val();
        var classe = $('#modifier_manuel_classe').val();
        var editeur = $('#modifier_manuel_editeur').val();
        var annee = $('#modifier_manuel_annee').val();
        if (tarif == "") {
            ok_ajout = false;
            $('#modifier_manuel_tarif').parent().addClass('error');
            message_erreur += "<li>Tarif</li>";
        }
        if (titre == "") {
            ok_ajout = false;
            $('#modifier_manuel_titre').parent().addClass('error');
            message_erreur += "<li>Titre</li>";
        }
        if (matiere == "") {
            ok_ajout = false;
            $('#modifier_manuel_matiere').parent().addClass('error');
            message_erreur += "<li>Matière</li>";
        }
        if (classe == "") {
            ok_ajout = false;
            $('#modifier_manuel_classe').parent().addClass('error');
            message_erreur += "<li>Classe</li>";
        }
        if (editeur == "") {
            ok_ajout = false;
            $('#modifier_manuel_editeur').parent().addClass('error');
            message_erreur += "<li>Editeur</li>";
        }
        if (annee == "") {
            ok_ajout = false;
            $('#modifier_manuel_annee').parent().addClass('error');
            message_erreur += "<li>Année d'édition</li>";
        }
        if (ok_ajout) {
            $.ajax({
                type: "PUT",
                url: "/api/manuels",
                data: {id: id, tarif: tarif, titre: titre, matiere: matiere, classe: classe, editeur: editeur, annee: annee}
            }).done(function (msg) {
                $('#modal_succes .content').html(msg.message);
                $('#modal_succes').modal('show');
            });
        } else {
            $('#modal_erreur .content').html('Veuillez compléter les champs suivants :' + message_erreur + "</ul>");
            $('#modal_erreur').modal('show');
        }
        $(this).removeClass('loading');

    });

    $('#button_supprimer_manuel').on('click', function (e) {
        var id = $('#modifier_manuel').data('id');
        e.preventDefault();
        $('#modal_confirm_supprimer').modal({
                closable  : false,
                onDeny    : function(){
                },
                onApprove : function() {
                    $.ajax({
                        type: "DELETE",
                        url: "/api/manuels/" + id
                    }).done(function (msg) {
                        window.location.assign("/");
                    });
                }
              })
              .modal('setting', 'closable', false)
              .modal('show');
    });
});